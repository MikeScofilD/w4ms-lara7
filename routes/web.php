<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');

Route::get('/contact', function () {
    return view('contact');
});

Route::post('/send-email', function () {
    return "/send-email";
})->name('contact');

Route::get('/page/about', 'PageController@show');

Route::resource('/posts', 'PostController');
