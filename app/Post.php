<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 * @package App
 * @mixin Builder
 */
class Post extends Model
{
    //protected $table = 'posts';
    //protected $primaryKey = 'post_id';
    //public $incrementing = false;
    //protected $keyType = 'string';
    //public $timestamps = false;
//    protected $attributes = [
//      'content' => 'Lorem ipsum...',
//    ];

    protected $fillable = ['title', 'content'];

    public function rubric(){
        return $this->belongsTo(Rubric::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }
}
