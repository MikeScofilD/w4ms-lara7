<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rubric extends Model
{
    public function post(){
//        $this->hasOne('App\Post');
        $this->hasOne(Post::class);
    }
    public function posts(){
        return $this->hasMany('App\Post');
    }
}
